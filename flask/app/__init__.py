# -*- coding: utf-8 -*-
import secrets
from app.Tools import utilities
from flask import Flask

# APP ROOT
app = Flask(__name__)

# APP CONFIGURATION
app.config["SECRET_KEY"] = secrets.token_urlsafe(16)
app.config['UPLOAD_FOLDER'] = utilities.UPLOAD_FOLDER
app.config['BASIC_AUTH_USERNAME'] = 'filrouge'
app.config['BASIC_AUTH_PASSWORD'] = 'Masteresio2021'
app.config['BASIC_AUTH_FORCE'] = True

# Import des routes
from app import routes
        