# importing os module
import os, shutil, csv, base64, boto3, json
import urllib.request as ur
from flask import escape, jsonify

# Importing Pillow Python Imaging Library
from pathlib import Path
from PIL import Image
from PIL.ExifTags import TAGS
from werkzeug.utils import secure_filename
from PyPDF2 import PdfFileReader
from openpyxl import load_workbook
from hachoir.parser import createParser
from hachoir.metadata import extractMetadata
from botocore.exceptions import ClientError


# Folder used to save uploaded image during the first processing
UPLOAD_FOLDER = Path("./app/temp")

# S3 Bucket name
BUCKET = "fil-rouge-bucket"

# Allowed extensions for the thumbnail generator (Trying to avoid XSS problems)
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg", "txt", "csv", "xlsx", "pdf", "gif"}

# Permet de tester la validité d'un nom de fichier par rapport aux extensions
def is_an_allowed_file(filename):
    """Method used to test the filename validity considering allowed extensions

    Args:
        filename (str): the filename

    Returns:
        [boolean]: Return True if the filename extension is present in allowed extensions (png, jpg, jpeg)
    """
    filename = escape(filename)

    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def is_an_image(filename):
    """Method used to test if the file is an image

    Args:
        filename (str): the filename

    Returns:
        [boolean]: Return True if the filename extension is present in allowed extensions (png, jpg, jpeg, gif)
    """
    filename = escape(filename)

    return "." in filename and filename.rsplit(".", 1)[1].lower() in {
        "png",
        "jpg",
        "jpeg",
        "gif",
    }


def is_a_text_file(filename):
    """Method used to test if the file is a text file

    Args:
        filename (str): the filename

    Returns:
        [boolean]: Return True if the filename extension is present in allowed extensions (txt, pdf)
    """
    filename = escape(filename)

    return "." in filename and filename.rsplit(".", 1)[1].lower() in {"txt", "pdf"}


def is_csv(filename):
    """Method used to test if the file is a csv or excel file

    Args:
        filename (str): the filename

    Returns:
        [boolean]: Return True if the filename extension is present in allowed extensions (csv, xslx)
    """
    filename = escape(filename)

    return "." in filename and filename.rsplit(".", 1)[1].lower() in {"csv", "xlsx"}


# Permet de retourner les informations (metadatas) d'un fichier
def getFileInfos(filename, request):
    """Method used to get file informations

    Args:
        filename (str): the filename

    Returns:
        [dict]: Return the dict containing the file informations
    """

    # 0 - Processing response dict
    infos = {}

    # 1 - Send file to aws s3 bucket if name is not empty
    name_in_bucket = str(secure_filename(request.files["file"].filename))

    if name_in_bucket:
        is_successfull = upload_to_s3(filename, name_in_bucket)

        if is_successfull:
            infos["Message"] = "File uploaded to AWS S3 Bucket 🟢!"

            # 2 - Construction des informations du JSON
            # --------------------------------------

            name = name_in_bucket
            length = str(request.headers.get("Content-Length"))
            file_format = name.split(".")[-1].lower()

            infos["Data"] = {
                "filename ": name,
                "length ": "{:.2} Ko".format(length),
                "format ": file_format,
            }

            infos["Metadata"] = {}

            if is_an_image(filename):
                image = Image.open(filename)
                infos["Data"]["size"] = f"{image.size[0]}px / {image.size[1]}px"

                # Metadata using Pillow
                infos["Metadata"].update(get_png_jpg_gif_info(filename))

                # Metadata using Hachoir
                if file_format == "jpg" or file_format == "jpeg":
                    infos["Metadata"].update(get_jpg_info(filename))

                # Metadata using AWS Rekognition
                url = "https://38yf716xld.execute-api.us-east-1.amazonaws.com/development/image-processing?bucket={}&filename={}".format(
                    BUCKET, name
                )
                response = ur.urlopen(url)
                data = json.loads(response.read())

                infos["Metadata"].update(data)

            elif is_a_text_file(filename):
                if file_format == "pdf":
                    infos["Metadata"].update(get_pdf_info(filename))

                else:
                    infos["Metadata"].update(get_text_info(filename))

                    # Metadata using AWS Comprehend
                    url = "https://38yf716xld.execute-api.us-east-1.amazonaws.com/development/text-processing?bucket={}&filename={}".format(
                        BUCKET, name
                    )
                    response = ur.urlopen(url)
                    data = json.loads(response.read())

                    infos["Metadata"].update(data)

            elif is_csv(filename):
                if file_format == "csv":
                    infos["Metadata"].update(get_csv_info(filename))

                else:
                    infos["Metadata"].update(get_xlsx_info(filename))

        else:
            infos["error"] = "Error while uploading file to AWS S3 Bucket"
    return infos


# Permet de supprimer le contenu des repetoirs passé en parametre
def clean_directory(path):
    """Method used to clean directory

    Args:
        path (Path): the path of the directory wich need to be cleaned

    Returns:
        Error message if the path is wrong
    """
    folder = path
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)

        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)

        except Exception as e:
            print("Failed to delete %s. Reason: %s" % (file_path, e))


def get_pdf_info(path):
    pages = 0

    with open(path, "rb") as f:
        pdf = PdfFileReader(f)
        info = pdf.getDocumentInfo()
        pages = pdf.getNumPages()

    author = info.author
    creator = info.creator
    producer = info.producer
    subject = info.subject
    title = info.title

    return {
        "author ": author,
        "creator ": creator,
        "producer ": producer,
        "subject ": subject,
        "title ": title,
        "pages ": pages,
    }


def get_text_info(path):  # TODO: Terminer ceci aussi
    # 1. Read the file
    file_ = open(path, "r")

    # 3. Close File
    file_.close()

    return {"Others Metadata": "On verra 😪"}


def get_xlsx_info(path):
    # 1. Read the file
    file_ = load_workbook(path)

    creator = str(file_.properties.creator)
    title = str(file_.properties.title)
    description = str(file_.properties.description)
    created_at = str(file_.properties.created)
    modified_at = str(file_.properties.modified)
    lastModified = str(file_.properties.lastModifiedBy)
    version = str(file_.properties.version)

    return {
        "creator ": creator,
        "title ": title,
        "description ": description,
        "created_at ": created_at,
        "modified_at ": modified_at,
        "lastModifier ": lastModified,
        "version ": version,
    }


def get_csv_info(path):
    information = []

    return {"comming ": "CSV"}


def get_jpg_info(path):
    # Instanciation d'un objet image
    try:
        image = Image.open(path)

        print(type(image))

        metadatas = {}

        # extract EXIF data
        exifdata = image.getexif()

        # iterating over all EXIF data fields
        for tag_id in exifdata:
            # get the tag name, instead of human unreadable tag id
            tag = TAGS.get(tag_id, tag_id)
            data = exifdata.get(tag_id)

            # decode bytes
            if isinstance(data, bytes):
                data = data.decode()

            metadatas[f"{tag:25}"] = f"{data}"

        return metadatas
    except UnicodeDecodeError as e:
        return {
            "Error": "Metadata pretty it's hard to retreive for this file 😅, {}".format(
                e
            )
        }


def get_png_jpg_gif_info(path):
    filename = path

    parser = createParser(filename)
    metadata = extractMetadata(parser)

    return metadata.exportDictionary()["Metadata"]


def upload_to_s3(file_name, object_name):
    # Open session with credential
    session = boto3.Session(profile_name="csloginstudent")
    s3_client = session.client("s3")

    response = ""
    try:
        response = s3_client.upload_file(file_name, BUCKET, object_name)
    except ClientError as e:
        logging.error(e)
        return False, response
    return True, response